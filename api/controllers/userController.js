const jwt = require("jsonwebtoken");

exports.login = (req, res, next) => {
  const token = jwt.sign(
    {
      email: req.body.email,
      userId: req.body.password
    },
    "secret",
    {
      expiresIn: "1h"
    }
  );
  jsonResposne(res, token, 200);
};

function jsonResposne(res, data, statusCode) {
  res.status(statusCode).json({
    DATA: data,
    CODE: "0000",
    MESSAGE: "Success"
  });
}
