var drinks = [
  { id: 1, name: "Bloody Mary", drunkness: 3 },
  { id: 2, name: "Martini", drunkness: 5 },
  { id: 3, name: "Scotch", drunkness: 10 }
];

// GET
exports.get_all_product = (req, res, next) => {
  jsonResposne(res, drinks, 200);
};

// GET
exports.find_one_product = (req, res, next) => {
  var obj = {};
  drinks.forEach(data => {
    if (data.id == req.params.productId) {
      obj = data;
    }
  });
  jsonResposne(res, obj, 200);
};

/**
 * {
     "id": 1,
     "name": "nora",
     "drinkness": 2
   }
 */
// POST
exports.add_product_api = (req, res, next) => {
  jsonResposne(res, req.body, 201);
};

// POST
exports.upload_product_image = (req, res, next) => {
  console.log(req.file);
  if (!req.file) {
    res.status(500);
    return next(err);
  }
  res.json({
    fileUrl: "/images/" + req.file.filename,
    destination: req.file.destination,
    size: req.file.size,
    originalname: req.file.originalname,
    extension: req.file.mimetype
  });
};

// POST
exports.upload_product_photo = (req, res, next) => {
  console.log("Photos ", typeof req.files);
  if (!req.files) {
    res.status(500);
    return next(err);
  }
  var arrFile = req.files;
  var index = 0;
  var rs = [];
  console.log("arr size: ", req.files[0].filename);
  while (index < arrFile.length) {
    rs.push({
      fileUrl: "/images/" + req.files[index].filename,
      destination: req.files[index].destination,
      size: req.files[index].size,
      originalname: req.files[index].originalname,
      extension: req.files[index].mimetype
    });
    index++;
  }
  res.json({ data: rs });
};

// Response
function jsonResposne(res, data, statusCode) {
  res.status(statusCode).json({
    DATA: data,
    CODE: "0000",
    MESSAGE: "Success"
  });
}
