/*
Module:multer
multer is middleware used to handle multipart form data
and it will create folder public/uploads
*/

const uuid = require("uuid/v1");
var multer = require("multer");

var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    //cb =callback funciton
    cb(null, "public/images/");
  },
  filename: (req, file, cb) => {
    console.log("storage: ", file);
    var filetype = file.originalname.substr(
      file.originalname.indexOf(".") + 1,
      file.originalname.length - 1
    );
    if (file.mimetype === "image/gif") {
      filetype = "gif";
    }
    if (file.mimetype === "image/png") {
      filetype = "png";
    }
    if (file.mimetype === "image/jpeg") {
      filetype = "jpg";
    }
    cb(null, Date.now() + "_" + uuid() + "." + filetype);
  }
});
var multerupload = multer({
  // dest: "public/images"
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  }
});

module.exports = multerupload;
