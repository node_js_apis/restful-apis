const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    // token get from header authorization
    const token = req.headers.authorization.split(" ")[1];
    console.log("otke", token);
    // get from request form-data
    //const decoded = jwt.verify(req.body.token, 'secret');
    const decoded = jwt.verify(token, "secret");
    req.userData = decoded;
    next();
  } catch (error) {
    return res.status(404).json({
      message: "Auth failed"
    });
  }
};
