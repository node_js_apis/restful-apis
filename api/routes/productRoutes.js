const express = require("express");
const router = express.Router();

const checkAuth = require("../middleware/check-auth");
const upload = require("../middleware/upload");
const productController = require("../controllers/productController");

router.get("/", productController.get_all_product);
router.get("/:productId", productController.find_one_product);
router.post("/", checkAuth, productController.add_product_api);
/* for upload url*/
router.post(
  "/upload",
  checkAuth,
  upload.single("file"),
  productController.upload_product_image
);

router.post(
  "/upload/photo",
  checkAuth,
  upload.array("files", 10),
  productController.upload_product_photo
);

module.exports = router;
