const express = require("express");
var bodyParser = require("body-parser");
const morgan = require("morgan");

const app = express();

const productRoutes = require("./api/routes/productRoutes");
const userRoutes = require("./api/routes/userRoutes");

// always called when start express
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(express.static("public"));

// config cors must be above of router
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Request-Headers",
    "origin, x-requested-with, accept, Content-Type, Authorization"
  );
  if (req.method == "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "POST, GET, PATCH, DELETE, PUT");
    return res.status(200).json({});
  }
  next();
});

app.use("/api/product", productRoutes);
app.use("/api/user", userRoutes);

// handle all request error
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      messsage: error.messsage
    }
  });
});

// export this model
module.exports = app;
